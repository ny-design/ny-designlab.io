function compIsFullScreen() {
	return (typeof document.fullscreenElement != "undefined" && document.fullscreenElement !== null) || (typeof document.webkitFullscreenElement != "undefined" && document.webkitFullscreenElement !== null) || (typeof document.mozFullScreenElement != "undefined" && document.mozFullScreenElement !== null) || (typeof document.msFullscreenElement != "undefined" && document.msFullscreenElement !== null);
}
function makeFullScreen(item) {
	if (!document.fullscreen && !document.mozFullScreen && !document.webkitIsFullScreen && !compIsFullScreen()) {
		if (item.requestFullscreen) {
			item.requestFullscreen();
		} else if (item.msRequestFullscreen) {
			item.msRequestFullscreen();               
		} else if (item.mozRequestFullScreen) {
			item.mozRequestFullScreen();      
		} else if (item.webkitRequestFullscreen) {
			item.webkitRequestFullscreen();       
		} else {
			console.log("Fullscreen API is not supported");
		}
	} else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else {
			console.log("Exit Fullscreen API is not supported");
		}
	}
}
function nothing(event) {
	event.preventDefault(); event.stopPropagation(); event.stopImmediatePropagation();
	event.returnValue = false;
	return false;
}
